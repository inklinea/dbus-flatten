# dbus flatten - An Inkscape 1.2.1 + extension.

Appears at `Extensions>dbus flatten`

Just a couple of dbus action tricks to force Inkscape to flatten transforms for selected objects.

If you are using Windows on a virtual machine or have a very slow windows machine, it may fail.

This can be rememdied by adding sleep delays in the code, but should not be necesary for most users.

There is an annoying popup window when the extension completes, I may fix this at a later date.
