#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# dbus flatten - a couple of tricks using dbus actions to flatten transforms
# An Inkscape 1.2.1+ extension
##############################################################################

import inkex

from ink_dbus import *

import sys

# Let's make a function to decend into groups
def apply_group_transforms(self, group):

    # We only want the 1st level of children
    direct_children = group.getchildren()

    if group.get('transform'):
        group_transform = group.transform

        for child in direct_children:
            # Apply the parent group transform to the child
            child.transform = group_transform @ child.transform
            child_transform_string = child.get('transform')
            InkDbus.ink_dbus_action(self, 'application', 'select-clear', '', '', None)
            InkDbus.ink_dbus_action(self, 'application', 'select-by-id', child.get_id(), 's', None)
            InkDbus.ink_dbus_action(self, 'application', 'object-set-attribute', f'transform,{child_transform_string}', 's', None)

        # Now remove the transform from the parent group
        group.set('transform', None)
        InkDbus.ink_dbus_action(self, 'application', 'select-clear', '', '', None)
        InkDbus.ink_dbus_action(self, 'application', 'select-by-id', group.get_id(), 's', None)
        InkDbus.ink_dbus_action(self, 'application', 'transform-remove', '','', None)

def process_all_groups(self, selection_list):

    group_list = []

    for selection in selection_list:
        for group in selection.xpath('.//svg:g'):
        # Let eliminate any duplicate groups

            group_list.append(group)
    list(set(group_list))

    for _group in group_list:
        apply_group_transforms(self, _group)


def flatten_path(self, element):
    InkDbus.ink_dbus_action(self, 'application', 'select-clear', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'select-by-id', element.get_id(), 's', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-to-path', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-flip-horizontal', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-flip-horizontal', '', '', None)


def flatten_text(self, text_element):
    InkDbus.ink_dbus_action(self, 'application', 'select-clear', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'select-by-id', text_element.get_id(), 's', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-flip-horizontal', '', '', None)
    InkDbus.ink_dbus_action(self, 'application', 'object-flip-horizontal', '', '', None)

def flatten_element_tree(self, root):

    for element in root.iter():

        if element.TAG != 'g':
            if element.get('transform'):
                if element.TAG != 'text':
                    flatten_path(self, element)
                else:
                    flatten_text(self, element)


class DbusFlatten(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--flatten_source_radio", type=str, dest="flatten_source_radio", default='selected')
    
    def effect(self):

        if self.options.flatten_source_radio == 'selected':
            selection_list = self.svg.selected
            if len(selection_list) < 1:
                sys.exit()
        else:
            selection_list = self.svg

        # First transfer all parent group transformations
        # onto direct children, then delete group transform

        for selected in selection_list:
            for element in selected.iter():
                if element.TAG == 'g':
                    apply_group_transforms(self, element)

        # Then force Inkscape to flatten each element
        for selected in selection_list:
            flatten_element_tree(self, selected)

        # Now exit the extension *without* returning the svg
        # from the extension system
        sys.exit()

if __name__ == '__main__':
    DbusFlatten().run()
